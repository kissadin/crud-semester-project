﻿// <copyright file="FencingEventsAtOlympicsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FencingAtOlympics.Data;

    /// <summary>
    /// Repository of the Events
    /// </summary>
    public class FencingEventsAtOlympicsRepository : IRepository<FencingEventsAtOlympic>
    {
        private FAOEntities db = new FAOEntities();

        /// <summary>
        /// Creates a FencingEventsAtOlympic entity
        /// </summary>
        /// <param name="obj">FencingEventsAtOlympic entity</param>
        public void Create(FencingEventsAtOlympic obj)
        {
            this.db.FencingEventsAtOlympics.Add(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes an event
        /// </summary>
        /// <param name="obj">event we want to delete</param>
        public void Delete(FencingEventsAtOlympic obj)
        {
            this.db.FencingEventsAtOlympics.Remove(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// deletes an event by ID
        /// </summary>
        /// <param name="id">ID of event to be deleted</param>
        public void Delete(int id)
        {
            this.db.FencingEventsAtOlympics.Remove(this.Read(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Reads a record of events
        /// </summary>
        /// <param name="id">ID of event to be read</param>
        /// <returns>returns an event</returns>
        public FencingEventsAtOlympic Read(int id)
        {
            return this.db.FencingEventsAtOlympics.First(f => f.ID == id);
        }

        /// <summary>
        /// Read everything from event
        /// </summary>
        /// <returns>returns all events</returns>
        public IEnumerable<FencingEventsAtOlympic> ReadAll()
        {
            return this.db.FencingEventsAtOlympics.AsQueryable();
        }

        /// <summary>
        /// updates an event
        /// </summary>
        /// <param name="obj">the event to be updated</param>
        public void Update(FencingEventsAtOlympic obj)
        {
            var toUpdate = this.db.FencingEventsAtOlympics.First(f => f.ID == obj.ID);
            if (toUpdate != null)
            {
                this.db.Entry(toUpdate).CurrentValues.SetValues(obj);
            }
            else
            {
                Console.WriteLine("Not found!");
            }
        }
    }
}
