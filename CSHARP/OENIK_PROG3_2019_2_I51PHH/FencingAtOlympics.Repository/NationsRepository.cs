﻿// <copyright file="NationsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FencingAtOlympics.Data;

    /// <summary>
    /// Repository of the Nations
    /// </summary>
    public class NationsRepository : IRepository<Nation>
    {
        /// <summary>
        /// Declaring a business logic
        /// </summary>
        private FAOEntities db = new FAOEntities();

        /// <summary>
        /// Creates a Nation entity
        /// </summary>
        /// <param name="obj">Nation object</param>
        public void Create(Nation obj)
        {
            this.db.Nations.Add(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a Nation entity
        /// </summary>
        /// <param name="obj">A Nation entity</param>
        public void Delete(Nation obj)
        {
            this.db.Nations.Remove(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a Nation entity (known by ID)
        /// </summary>
        /// <param name="id">ID of entity</param>
        public void Delete(int id)
        {
            this.db.Nations.Remove(this.Read(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Read a Nation entity
        /// </summary>
        /// <param name="id">ID of a Nation</param>
        /// <returns>Returns a Nation entity</returns>
        public Nation Read(int id)
        {
            return this.db.Nations.First(t => t.ID == id);
        }

        /// <summary>
        /// Read all of the Nation entities
        /// </summary>
        /// <returns>Returns all Nations</returns>
        public IEnumerable<Nation> ReadAll()
        {
            return this.db.Nations.AsQueryable();
        }

        /// <summary>
        /// Update one Nation entity
        /// </summary>
        /// <param name="obj">Nation entity (that we want to update)</param>
        public void Update(Nation obj)
        {
            var toUpdate = this.db.Nations.First(n => n.ID == obj.ID);
            if (toUpdate != null)
            {
                this.db.Entry(toUpdate).CurrentValues.SetValues(obj);
            }
            else
            {
                Console.WriteLine("Not found!");
            }
        }
    }
}
