﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Required methods for the repo (CRUD)
    /// </summary>
    /// <typeparam name="T">Generic</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Read all data from a table of the DB
        /// </summary>
        /// <returns>All data of DB</returns>
        IEnumerable<T> ReadAll();

        /// <summary>
        /// Read data from a table of the DB
        /// </summary>
        /// <param name="id">id of a record</param>
        /// <returns>Type T, known by its id</returns>
        T Read(int id);

        /// <summary>
        /// Creates a new record
        /// </summary>
        /// <param name="obj">the record we want to create</param>
        void Create(T obj);

        /// <summary>
        /// Update an existing record
        /// </summary>
        /// <param name="obj">The rcord we want to modify</param>
        void Update(T obj);

        /// <summary>
        /// Deletes a record
        /// </summary>
        /// <param name="obj">The record we want to delete</param>
        void Delete(T obj);

        /// <summary>
        /// Deletes a record, known by its id
        /// </summary>
        /// <param name="id">ID if the record we want to delete</param>
        void Delete(int id);
    }
}
