﻿// <copyright file="OlympicsHostRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FencingAtOlympics.Data;

    /// <summary>
    /// Repository of the Host cities
    /// </summary>
    public class OlympicsHostRepository : IRepository<OlympicsHost>
    {
        /// <summary>
        /// Declaring a business logic
        /// </summary>
        private FAOEntities db = new FAOEntities();

        /// <summary>
        /// Creates a host city
        /// </summary>
        /// <param name="obj">olympicsHost that we want to create</param>
        public void Create(OlympicsHost obj)
        {
            this.db.OlympicsHost.Add(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a host city
        /// </summary>
        /// <param name="obj">the host city to be deleted</param>
        public void Delete(OlympicsHost obj)
        {
            this.db.OlympicsHost.Remove(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a host city (known by ID)
        /// </summary>
        /// <param name="id">the host city to be deleted (known by ID)</param>
        public void Delete(int id)
        {
            this.db.OlympicsHost.Remove(this.Read(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Reads a host city
        /// </summary>
        /// <param name="id">ID of host city</param>
        /// <returns>returns a host city</returns>
        public OlympicsHost Read(int id)
        {
            return this.db.OlympicsHost.First(t => t.ID == id);
        }

        /// <summary>
        /// Read every host city
        /// </summary>
        /// <returns>returns all host cities</returns>
        public IEnumerable<OlympicsHost> ReadAll()
        {
            return this.db.OlympicsHost.AsQueryable();
        }

        /// <summary>
        /// updates a host city
        /// </summary>
        /// <param name="obj">the host city to be updated</param>
        public void Update(OlympicsHost obj)
        {
            var toUpdate = this.db.OlympicsHost.First(o => o.ID == obj.ID);
            if (toUpdate != null)
            {
                this.db.Entry(toUpdate).CurrentValues.SetValues(obj);
            }
            else
            {
                Console.WriteLine("Not found!");
            }
        }
    }
}
