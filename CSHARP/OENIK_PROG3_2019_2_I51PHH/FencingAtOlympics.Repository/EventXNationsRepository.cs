﻿// <copyright file="EventXNationsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FencingAtOlympics.Data;

    /// <summary>
    /// Repository of the connection table
    /// </summary>
    public class EventXNationsRepository : IRepository<EventXNation>
    {
        /// <summary>
        /// Declaring a business logic
        /// </summary>
        private FAOEntities db = new FAOEntities();

        /// <summary>
        /// Creates a connection
        /// </summary>
        /// <param name="obj">EventXNation object</param>
        public void Create(EventXNation obj)
        {
            this.db.EventXNations.Add(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a connection
        /// </summary>
        /// <param name="obj">Connection to be deleted</param>
        public void Delete(EventXNation obj)
        {
            this.db.EventXNations.Remove(obj);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a connection
        /// </summary>
        /// <param name="id">ID of connection we want to delete</param>
        public void Delete(int id)
        {
            this.db.EventXNations.Remove(this.Read(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Reads a connection
        /// </summary>
        /// <param name="id">id of connection</param>
        /// <returns>returns a connection</returns>
        public EventXNation Read(int id)
        {
            return this.db.EventXNations.First(e => e.ID == id);
        }

        /// <summary>
        /// Read all connection
        /// </summary>
        /// <returns>returns all connections</returns>
        public IEnumerable<EventXNation> ReadAll()
        {
            return this.db.EventXNations.AsQueryable();
        }

        /// <summary>
        /// updates a connection
        /// </summary>
        /// <param name="obj">the connection we want to update</param>
        public void Update(EventXNation obj)
        {
            var toUpdate = this.db.EventXNations.First(e => e.ID == obj.ID);
            if (toUpdate != null)
            {
                this.db.Entry(toUpdate).CurrentValues.SetValues(obj);
            }
        }
    }
}
