﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FencingAtOlympics.Data;
    using FencingAtOlympics.Logic;

    /// <summary>
    /// This is the Program class in FencingAtOlympics.Program namespace
    /// </summary>
    public class Program
    {
        /// <summary>
        /// This is the Main method in Program class
        /// </summary>
        /// <param name="args">args param in Main method</param>
        public static void Main(string[] args)
        {
            while (UpperMenu() != "7")
            {
            }

            Console.Clear();
            Console.WriteLine("Bye!");
            Console.ReadLine();
        }

        /// <summary>
        /// The menu wich is used to navigate in the program
        /// You can choose the non-CRUD and JAVA methods from here
        /// You can navigate to the CRUD methods from here.
        /// </summary>
        /// <returns>choosen</returns>
        private static string UpperMenu()
        {
            Console.Clear();
            Console.WriteLine("Wich table do you choose to work in?");
            Console.WriteLine("[1] Host cities");
            Console.WriteLine("[2] Events");
            Console.WriteLine("[3] Nations");
            Console.WriteLine("[4] not crud 1");
            Console.WriteLine("[5] not crud 2");
            Console.WriteLine("[6] not crud 3");
            Console.WriteLine("[7] Exit");
            Console.Write("You choose: ");
            string choosen = Console.ReadLine();
            Logic lg = new Logic();
            switch (choosen)
            {
                case "1":
                    List<string> hostCities = new List<string>
                    {
                        "ID",
                        "City",
                        "Country",
                        "Continent",
                        "CountryCode",
                        "Comment"
                    };
                    MenuHostCities(lg, hostCities);
                    break;
                case "2":
                    List<string> events = new List<string>
                    {
                        "ID",
                        "Weapon",
                        "Sex",
                        "Team_Individual",
                        "YearOfEvent",
                        "OlympicHostID",
                    };
                    MenuEvents(lg, events);
                    break;
                case "3":
                    List<string> nations = new List<string>
                    {
                        "ID",
                        "Nation",
                        "Anthem",
                        "NumOfFencers",
                        "Gold",
                        "Silver",
                        "Bronze",
                    };
                    MenuNations(lg, nations);
                    break;
                case "4":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "5":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "6":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
            }

            return choosen;
        }

        /// <summary>
        /// The CRUDs of table of the host cities can be choosen from here.
        /// </summary>
        /// <param name="lg">business logic</param>
        /// <param name="hostCities">field names of table</param>
        private static void MenuHostCities(Logic lg, List<string> hostCities)
        {
            Console.Clear();
            Console.WriteLine("What do you want to do?");
            Console.WriteLine("[1] Create");
            Console.WriteLine("[2] Read");
            Console.WriteLine("[3] Read all");
            Console.WriteLine("[4] Update");
            Console.WriteLine("[5] Delete");
            Console.Write("You choose: ");
            string choosenCRUD = Console.ReadLine();
            Console.Clear();
            switch (choosenCRUD)
            {
                case "1":
                    List<string> createTemp = GatherTheData(hostCities.Count, hostCities);
                    OlympicsHost o = new OlympicsHost
                    {
                        ID = int.Parse(createTemp.ElementAt(0)),
                        City = createTemp.ElementAt(1),
                        Country = createTemp.ElementAt(2),
                        Continent = createTemp.ElementAt(3),
                        CountryCode = createTemp.ElementAt(4),
                        Comment = createTemp.ElementAt(5)
                    };
                    lg.CreateHostCity(o);
                    break;
                case "2":
                    int id = GatherID();
                    var readTemp = lg.ReadHostCity(id);
                    Console.WriteLine("\n" +
                        readTemp.ID + " | " +
                        readTemp.City + " | " +
                        readTemp.Country + " | " +
                        readTemp.Continent + " | " +
                        readTemp.CountryCode + " | " +
                        readTemp.Comment);
                    //System.Threading.Thread.Sleep(2000);
                    break;
                case "3":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "4":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "5":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
            }
        }

        /// <summary>
        /// The CRUDs of table of the events can be choosen from here.
        /// </summary>
        /// <param name="lg">business logic</param>
        /// <param name="events">field names of table</param>
        private static void MenuEvents(Logic lg, List<string> events)
        {
            Console.Clear();
            Console.WriteLine("What do you want to do?");
            Console.WriteLine("[1] Create");
            Console.WriteLine("[2] Read");
            Console.WriteLine("[3] Read all");
            Console.WriteLine("[4] Update");
            Console.WriteLine("[5] Delete");
            Console.Write("You choose: ");
            string choosenCRUD = Console.ReadLine();
            Console.Clear();
            switch (choosenCRUD)
            {
                case "1":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "2":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "3":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "4":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "5":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
            }
        }

        /// <summary>
        /// The CRUDs of table of the nations can be choosen from here.
        /// </summary>
        /// <param name="lg">business logic</param>
        /// <param name="nations">field names of table</param>
        private static void MenuNations(Logic lg, List<string> nations)
        {
            Console.Clear();
            Console.WriteLine("What do you want to do?");
            Console.WriteLine("[1] Create");
            Console.WriteLine("[2] Read");
            Console.WriteLine("[3] Read all");
            Console.WriteLine("[4] Update");
            Console.WriteLine("[5] Delete");
            Console.Write("You choose: ");
            string choosenCRUD = Console.ReadLine();
            Console.Clear();
            switch (choosenCRUD)
            {
                case "1":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "2":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "3":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "4":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
                case "5":
                    Console.WriteLine("Ez még nincs kész");
                    Console.ReadLine();
                    break;
            }
        }

        /// <summary>
        /// Create, Update and Delete methods use the data gathered here.
        /// </summary>
        /// <param name="num">fields number</param>
        /// <param name="fields">list of the names of the fields</param>
        /// <returns>returns the list of the data of the table</returns>
        private static List<string> GatherTheData(int num, List<string> fields)
        {
            List<string> temp = new List<string>();
            for (int i = 0; i < num; i++)
            {
                Console.WriteLine(fields.ElementAt(i) + ": ");
                temp.Add(Console.ReadLine());
            }

            return temp;
        }

        private static int GatherID()
        {
            string idString = string.Empty;
            int idInt;
            while (!int.TryParse(idString, out idInt))
            {
                Console.WriteLine("ID of element:");
                idString = Console.ReadLine();
            }

            return idInt;
        }
    }
}
