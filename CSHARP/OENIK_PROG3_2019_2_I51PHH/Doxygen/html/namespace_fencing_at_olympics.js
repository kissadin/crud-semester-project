var namespace_fencing_at_olympics =
[
    [ "Data", "namespace_fencing_at_olympics_1_1_data.html", "namespace_fencing_at_olympics_1_1_data" ],
    [ "Logic", "namespace_fencing_at_olympics_1_1_logic.html", "namespace_fencing_at_olympics_1_1_logic" ],
    [ "Program", "namespace_fencing_at_olympics_1_1_program.html", "namespace_fencing_at_olympics_1_1_program" ],
    [ "Repository", "namespace_fencing_at_olympics_1_1_repository.html", "namespace_fencing_at_olympics_1_1_repository" ]
];