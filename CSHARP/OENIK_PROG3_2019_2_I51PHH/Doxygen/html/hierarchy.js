var hierarchy =
[
    [ "FencingAtOlympics.Data.Data", "class_fencing_at_olympics_1_1_data_1_1_data.html", null ],
    [ "DbContext", null, [
      [ "FencingAtOlympics.Data.ModelFencingAtOlympics", "class_fencing_at_olympics_1_1_data_1_1_model_fencing_at_olympics.html", null ]
    ] ],
    [ "FencingAtOlympics.Data.FencingEventsAtOlympics", "class_fencing_at_olympics_1_1_data_1_1_fencing_events_at_olympics.html", null ],
    [ "FencingAtOlympics.Logic.Logic", "class_fencing_at_olympics_1_1_logic_1_1_logic.html", null ],
    [ "FencingAtOlympics.Data.OlympicsHost", "class_fencing_at_olympics_1_1_data_1_1_olympics_host.html", null ],
    [ "FencingAtOlympics.Program.Program", "class_fencing_at_olympics_1_1_program_1_1_program.html", null ],
    [ "FencingAtOlympics.Repository.Repository", "class_fencing_at_olympics_1_1_repository_1_1_repository.html", null ],
    [ "FencingAtOlympics.Logic.Tests.Tests", "class_fencing_at_olympics_1_1_logic_1_1_tests_1_1_tests.html", null ]
];