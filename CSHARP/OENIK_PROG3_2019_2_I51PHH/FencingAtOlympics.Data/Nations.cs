namespace FencingAtOlympics.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Nations
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Nations()
        {
            EventXNations = new HashSet<EventXNations>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Required]
        [StringLength(55)]
        public string Nation { get; set; }

        [StringLength(55)]
        public string Anthem { get; set; }

        public int? NumOfFencers { get; set; }

        public int? Gold { get; set; }

        public int? Silver { get; set; }

        public int? Bronze { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventXNations> EventXNations { get; set; }
    }
}
