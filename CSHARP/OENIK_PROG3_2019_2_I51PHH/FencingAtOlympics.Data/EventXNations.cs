namespace FencingAtOlympics.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EventXNations
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int EventID { get; set; }

        public int NationsID { get; set; }

        public virtual FencingEventsAtOlympics FencingEventsAtOlympics { get; set; }

        public virtual Nations Nations { get; set; }
    }
}
