namespace FencingAtOlympics.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FencingAtOlympicsModel : DbContext
    {
        public FencingAtOlympicsModel()
            : base("name=FencingAtOlympicsModel")
        {
        }

        public virtual DbSet<EventXNations> EventXNations { get; set; }
        public virtual DbSet<FencingEventsAtOlympics> FencingEventsAtOlympics { get; set; }
        public virtual DbSet<Nations> Nations { get; set; }
        public virtual DbSet<OlympicsHost> OlympicsHost { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FencingEventsAtOlympics>()
                .HasMany(e => e.EventXNations)
                .WithRequired(e => e.FencingEventsAtOlympics)
                .HasForeignKey(e => e.EventID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nations>()
                .Property(e => e.Nation)
                .IsUnicode(false);

            modelBuilder.Entity<Nations>()
                .Property(e => e.Anthem)
                .IsUnicode(false);

            modelBuilder.Entity<Nations>()
                .HasMany(e => e.EventXNations)
                .WithRequired(e => e.Nations)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OlympicsHost>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<OlympicsHost>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<OlympicsHost>()
                .Property(e => e.Continent)
                .IsUnicode(false);

            modelBuilder.Entity<OlympicsHost>()
                .Property(e => e.CountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<OlympicsHost>()
                .Property(e => e.Comment)
                .IsUnicode(false);

            modelBuilder.Entity<OlympicsHost>()
                .HasMany(e => e.FencingEventsAtOlympics)
                .WithOptional(e => e.OlympicsHost)
                .HasForeignKey(e => e.OlympicHostID);
        }
    }
}
