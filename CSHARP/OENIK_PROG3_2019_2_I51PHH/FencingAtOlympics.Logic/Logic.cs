﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Logic
{
    using System.Collections.Generic;
    using FencingAtOlympics.Data;
    using FencingAtOlympics.Repository;

    /// <summary>
    /// Logic class in FencingAtOlympics.Logic
    /// </summary>
    public class Logic : ILogic
    {
        /// <summary>
        /// OlympicsHostRepository
        /// </summary>
        private IRepository<OlympicsHost> olympicsHostRepository;

        /// <summary>
        /// FencingEventsAtOlympicsRepository
        /// </summary>
        private IRepository<FencingEventsAtOlympic> fencingEventsAtOlympicsRepository;

        /// <summary>
        /// NationsRepository
        /// </summary>
        private IRepository<Nation> nationsRepository;

        /// <summary>
        /// EventXNationsRepository
        /// </summary>
        private IRepository<EventXNation> eventXNationsRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// 1. ctro - host cities repo: class is reusable
        /// </summary>
        /// <param name="olympicsHostRepository">the olympicsHost Repository</param>
        public Logic(IRepository<OlympicsHost> olympicsHostRepository)
        {
            this.olympicsHostRepository = olympicsHostRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// 2. ctro - events repo: class is reusable
        /// </summary>
        /// <param name="fencingEventsAtOlympicsRepository">the fencing Events At Olympics Repository</param>
        public Logic(IRepository<FencingEventsAtOlympic> fencingEventsAtOlympicsRepository)
        {
            this.fencingEventsAtOlympicsRepository = fencingEventsAtOlympicsRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// 3. ctro - nations repo: class is reusable
        /// </summary>
        /// <param name="nationsRepository">the nations Repository</param>
        public Logic(IRepository<Nation> nationsRepository)
        {
            this.nationsRepository = nationsRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// 4. ctro - connection repo: class is reusable
        /// </summary>
        /// <param name="eventXNationsRepository">connection tables repository</param>
        public Logic(IRepository<EventXNation> eventXNationsRepository)
        {
            this.eventXNationsRepository = eventXNationsRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// 5. ctro - all repos: class is reusable
        /// </summary>
        /// <param name="olympicsHostRepository">the olympicsHost Repository</param>
        /// <param name="fencingEventsAtOlympicsRepository">the fencing Events At Olympics Repository</param>
        /// <param name="nationsRepository">the nations Repository</param>
        /// <param name="eventXNationsRepository">connection tables repository</param>
        public Logic(
            IRepository<OlympicsHost> olympicsHostRepository,
            IRepository<FencingEventsAtOlympic> fencingEventsAtOlympicsRepository,
            IRepository<Nation> nationsRepository,
            IRepository<EventXNation> eventXNationsRepository)
        {
            this.olympicsHostRepository = olympicsHostRepository;
            this.fencingEventsAtOlympicsRepository = fencingEventsAtOlympicsRepository;
            this.nationsRepository = nationsRepository;
            this.eventXNationsRepository = eventXNationsRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// 6. ctro - no repos: class is reusable
        /// </summary>
        public Logic()
        {
            this.olympicsHostRepository = new OlympicsHostRepository();
            this.fencingEventsAtOlympicsRepository = new FencingEventsAtOlympicsRepository();
            this.nationsRepository = new NationsRepository();
            this.eventXNationsRepository = new EventXNationsRepository();
        }

        // Host cities CRUD

        /// <summary>
        /// Calls the ReadAll() method of OlympicsHostRepository
        /// </summary>
        /// <returns>All of the host cities</returns>
        public IEnumerable<OlympicsHost> ReadAllHostCity()
        {
            return this.olympicsHostRepository.ReadAll();
        }

        /// <summary>
        /// Calls the Read() method of OlympicsHostRepository
        /// </summary>
        /// <param name="id">ID of host city</param>
        /// <returns>return a host city by ID</returns>
        public OlympicsHost ReadHostCity(int id)
        {
            return this.olympicsHostRepository.Read(id);
        }

        /// <summary>
        /// Calls the Create() method of OlympicsHostRepository
        /// </summary>
        /// <param name="obj">a host city entity</param>
        public void CreateHostCity(OlympicsHost obj)
        {
            this.olympicsHostRepository.Create(obj);
        }

        /// <summary>
        /// Calls the Update() method of OlympicsHostRepository
        /// </summary>
        /// <param name="obj">a host city entity</param>
        public void UpdateHostCity(OlympicsHost obj)
        {
            this.olympicsHostRepository.Update(obj);
        }

        /// <summary>
        /// Calls the Delete() method of OlympicsHostRepository
        /// </summary>
        /// <param name="obj">a host city entity</param>
        public void DeleteHostCity(OlympicsHost obj)
        {
            this.olympicsHostRepository.Delete(obj);
        }

        /// <summary>
        /// Calls the Delete() method of OlympicsHostRepository
        /// </summary>
        /// <param name="id">ID of a host city</param>
        public void DeleteHostCity(int id)
        {
            this.olympicsHostRepository.Delete(id);
        }

        // Events CRUD

        /// <summary>
        /// Calls the ReadAll() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <returns>returns all events</returns>
        public IEnumerable<FencingEventsAtOlympic> ReadAllEvents()
        {
            return this.fencingEventsAtOlympicsRepository.ReadAll();
        }

        /// <summary>
        /// Calls the Read() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="id">ID of an event</param>
        /// <returns>returns an event</returns>
        public FencingEventsAtOlympic ReadEvents(int id)
        {
            return this.fencingEventsAtOlympicsRepository.Read(id);
        }

        /// <summary>
        /// Calls the Create() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="obj">an event entity</param>
        public void CreateEvents(FencingEventsAtOlympic obj)
        {
            this.fencingEventsAtOlympicsRepository.Create(obj);
        }

        /// <summary>
        /// Calls the Update() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="obj">an event entity</param>
        public void UpdateEvents(FencingEventsAtOlympic obj)
        {
            this.fencingEventsAtOlympicsRepository.Update(obj);
        }

        /// <summary>
        /// Calls the Delete() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="obj">an event entity</param>
        public void DeleteEvents(FencingEventsAtOlympic obj)
        {
            this.fencingEventsAtOlympicsRepository.Delete(obj);
        }

        /// <summary>
        /// Calls the Delete() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="id">ID of an event</param>
        public void DeleteEvents(int id)
        {
            this.fencingEventsAtOlympicsRepository.Delete(id);
        }

        // Nations CRUD

        /// <summary>
        /// Calls the ReadAll() method of NationsRepository
        /// </summary>
        /// <returns>returns all nations</returns>
        public IEnumerable<Nation> ReadAllNations()
        {
            return this.nationsRepository.ReadAll();
        }

        /// <summary>
        /// Calls the Read() method of NationsRepository
        /// </summary>
        /// <param name="id">ID of nation</param>
        /// <returns>returns a nation</returns>
        public Nation ReadNations(int id)
        {
            return this.nationsRepository.Read(id);
        }

        /// <summary>
        /// Calls the Create() method of NationsRepository
        /// </summary>
        /// <param name="obj">Nation entity</param>
        public void CreateNations(Nation obj)
        {
            this.nationsRepository.Create(obj);
        }

        /// <summary>
        /// Calls the Update() method of NationsRepository
        /// </summary>
        /// <param name="obj">Nation entity</param>
        public void UpdateNations(Nation obj)
        {
            this.nationsRepository.Update(obj);
        }

        /// <summary>
        /// Calls the Delete() method of NationsRepository
        /// </summary>
        /// <param name="obj">Nation entity</param>
        public void DeleteNations(Nation obj)
        {
            this.nationsRepository.Delete(obj);
        }

        /// <summary>
        /// Calls the Delete() method of NationsRepository
        /// </summary>
        /// <param name="id">ID of nation</param>
        public void DeleteNations(int id)
        {
            this.nationsRepository.Delete(id);
        }
    }
}
