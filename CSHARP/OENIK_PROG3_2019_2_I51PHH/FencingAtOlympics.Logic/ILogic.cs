﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FencingAtOlympics.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FencingAtOlympics.Data;

    /// <summary>
    /// ILogic interface required methods for the business logic class.
    /// </summary>
    public interface ILogic
    {
        // Host cities CRUD

        /// <summary>
        /// Calls the ReadAll() method of OlympicHostRepository
        /// </summary>
        /// <returns>All Host cities</returns>
        IEnumerable<OlympicsHost> ReadAllHostCity();

        /// <summary>
        /// Calls the Read() method of OlympicHostRepository
        /// </summary>
        /// <param name="id">ID of host city</param>
        /// <returns>a host city with all its data</returns>
        OlympicsHost ReadHostCity(int id);

        /// <summary>
        /// Calls the Create() method of OlympicHostRepository
        /// </summary>
        /// <param name="obj">host city entity</param>
        void CreateHostCity(OlympicsHost obj);

        /// <summary>
        /// Calls the Update() method of OlympicHostRepository
        /// </summary>
        /// <param name="obj">host city entity</param>
        void UpdateHostCity(OlympicsHost obj);

        /// <summary>
        /// Calls the Delete() method of OlympicHostRepository
        /// </summary>
        /// <param name="obj">host city entity</param>
        void DeleteHostCity(OlympicsHost obj);

        /// <summary>
        /// Calls the Delete() method of OlympicHostRepository
        /// </summary>
        /// <param name="id">ID of a host city</param>
        void DeleteHostCity(int id);

        // Events CRUD

        /// <summary>
        /// Calls the ReadAllEvents() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <returns>Every event</returns>
        IEnumerable<FencingEventsAtOlympic> ReadAllEvents();

        /// <summary>
        /// Calls the Read() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="id">ID of event</param>
        /// <returns>an event</returns>
        FencingEventsAtOlympic ReadEvents(int id);

        /// <summary>
        /// Calls the Create() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="obj">An event</param>
        void CreateEvents(FencingEventsAtOlympic obj);

        /// <summary>
        /// Calls the Update() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="obj">An event</param>
        void UpdateEvents(FencingEventsAtOlympic obj);

        /// <summary>
        /// Calls the Delete() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="obj">An event</param>
        void DeleteEvents(FencingEventsAtOlympic obj);

        /// <summary>
        /// Calls the Delete() method of FencingEventsAtOlympicsRepository
        /// </summary>
        /// <param name="id">ID of an event</param>
        void DeleteEvents(int id);

        // Nations CRUD

        /// <summary>
        /// Calls the ReadAll() method of NationsRepository
        /// </summary>
        /// <returns>All the nations</returns>
        IEnumerable<Nation> ReadAllNations();

        /// <summary>
        /// Calls the Read() method of NationsRepository
        /// </summary>
        /// <param name="id">ID of a nation</param>
        /// <returns>A nation</returns>
        Nation ReadNations(int id);

        /// <summary>
        /// Calls the Create() method of NationsRepository
        /// </summary>
        /// <param name="obj">a nation</param>
        void CreateNations(Nation obj);

        /// <summary>
        /// Calls the Update() method of NationsRepository
        /// </summary>
        /// <param name="obj">a nation</param>
        void UpdateNations(Nation obj);

        /// <summary>
        /// Calls the Delete() method of NationsRepository
        /// </summary>
        /// <param name="obj">a nation</param>
        void DeleteNations(Nation obj);

        /// <summary>
        /// Calls the Delete() method of NationsRepository
        /// </summary>
        /// <param name="id">ID of a nation</param>
        void DeleteNations(int id);

        // Not CRUD

        // Not CRUD 1

        // Not CRUD 2

        // Not CRUD 3
    }
}
